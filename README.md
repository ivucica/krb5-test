Minimal way to obtain Kerberos 5 ticket data.

Create service principal:

    $ samba-tool user add krb5-test
    $ samba-tool spn add krb5-test/$(hostname) krb5-test

If running on same host (i.e. on `$(hostname)`) no need for:

    $ samba-tool domain exportkeytab ./test.keytab --principal=krb5-test/${other.hostname}@${DOMAIN.NAME}
    $ scp test.keytab ${other.hostname}:/etc/krb5.keytab
