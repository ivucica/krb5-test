#include <err.h>
#include <krb5.h>
#include <stdio.h>

void krb5_err(krb5_context const c,
              int const er, int const status, const char * const ert) {
  const char * ert2 = krb5_get_error_message(c, er);
  errx(er, "%s: %s", ert, ert2);
  // krb5_free_error_message() not called because errx exits
}

int main()
{
  krb5_context context;
  krb5_auth_context auth_context;
  int status;
  krb5_principal server;
  krb5_principal client;

  if (krb5_init_context(&context))
    errx (1, "krb5_context");
  status = krb5_auth_con_init (context, &auth_context);
  if (status)
    krb5_err (context, 1, status, "krb5_auth_con_init");

  const char * const hostname = "commander.ds.badc0de.net";
  const char * const service = "krb5-test";
  status = krb5_sname_to_principal (context,
                                    hostname,
                                    service,
                                    KRB5_NT_SRV_HST,
                                   &server);
  if (status)
    krb5_err (context, 1, status, "krb5_sname_to_principal");

  krb5_ccache cache;
  status = krb5_cc_default(context, &cache);
  if (status)
    krb5_err (context, 1, status, "krb5_cc_default");

  status = krb5_cc_get_principal(context, cache, &client);
  if (status)
    krb5_err (context, 1, status, "krb5_cc_get_principal");

  krb5_creds increds = {
    .server = server,
    .client = client,
  };
  krb5_creds * outcreds;
  krb5_error_code ercode = krb5_get_credentials(
      context, /* options */ 0, cache, &increds, &outcreds);
  if (ercode)
    krb5_err (context, 1, ercode, "krb5_get_credentials");
  /* Intentionally, but brokenly, treating the following as a
     string. This means this ignores ticket.length etc. */
  printf("ticket: %s\n", outcreds->ticket.data);
  krb5_free_creds(context, outcreds);
}
